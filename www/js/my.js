// From https://threejs.org/docs/index.html#manual/introduction/Creating-a-scene
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

// var geometry = new THREE.BoxGeometry( 1, 1, 1 );
// var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
// var cube = new THREE.Mesh( geometry, material );
// scene.add( cube );

// camera.position.z = 5;

// //If you copied the code from above into the HTML file we created earlier, you wouldn't be able to see anything. 
// //This is because we're not actually rendering anything yet. For that, we need what's called a render or animate loop.

// This will create a loop that causes the renderer to draw the scene 60 times per second. 
// If you're new to writing games in the browser, you might say "why don't we just create a setInterval? 
// The thing is - we could, but requestAnimationFrame has a number of advantages. 
// Perhaps the most important one is that it pauses when the user navigates to another browser tab, 
// hence not wasting their precious processing power and battery life.

// This will be run every frame (60 times per second), and give the cube a nice rotation animation. 
// Basically, anything you want to move or change while the app is running has to go through the animate loop. 
// You can of course call other functions from there, so that you don't end up with a animate function that's hundreds of lines. 

// var animate = function () {
// 	requestAnimationFrame( animate );

// 	// cube.rotation.x += 0.1;
// 	// cube.rotation.y += 0.1;

// 	renderer.render(scene, camera);
// };

// animate();

// cube.rotation.x += 0.4;
// cube.rotation.y += 0.4;

// //create a blue LineBasicMaterial
// var material = new THREE.LineBasicMaterial({ color: 0xffffff });

// var geometry = new THREE.Geometry();
// geometry.vertices.push(new THREE.Vector3(0, 0, 0));
// geometry.vertices.push(new THREE.Vector3(0, 1, 0));
// // geometry.vertices.push(new THREE.Vector3(10, 30, 30));

// var line = new THREE.Line(geometry, material);

// scene.add(line);
// renderer.render(scene, camera);

//==================================================================Insert images into faces ======================================================================================
var textureLoader = new THREE.TextureLoader();

var texture0 = textureLoader.load( 'img/paper-box-texture.jpg' );
var texture1 = textureLoader.load( 'img/paper-box-texture.jpg' );
var texture2 = textureLoader.load( 'img/paper-box-texture.jpg' );
var texture3 = textureLoader.load( 'img/paper-box-texture.jpg' );
var texture4 = textureLoader.load( 'img/paper-box-texture.jpg' );
var texture5 = textureLoader.load( 'img/paper-box-texture.jpg' );

var materials = [
    new THREE.MeshBasicMaterial( { map: texture0 } ),
    new THREE.MeshBasicMaterial( { map: texture1 } ),
    new THREE.MeshBasicMaterial( { map: texture2 } ),
    new THREE.MeshBasicMaterial( { map: texture3 } ),
    new THREE.MeshBasicMaterial( { map: texture4 } ),
    new THREE.MeshBasicMaterial( { map: texture5 } )
];
var faceMaterial = new THREE.MeshFaceMaterial( materials );

var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var box = new THREE.Mesh( geometry, faceMaterial );
scene.add(box);

camera.position.z = 5;

//If you copied the code from above into the HTML file we created earlier, you wouldn't be able to see anything. 
//This is because we're not actually rendering anything yet. For that, we need what's called a render or animate loop.
/*
This will create a loop that causes the renderer to draw the scene 60 times per second. 
If you're new to writing games in the browser, you might say "why don't we just create a setInterval? 
The thing is - we could, but requestAnimationFrame has a number of advantages. 
Perhaps the most important one is that it pauses when the user navigates to another browser tab, 
hence not wasting their precious processing power and battery life.

This will be run every frame (60 times per second), and give the cube a nice rotation animation. 
Basically, anything you want to move or change while the app is running has to go through the animate loop. 
You can of course call other functions from there, so that you don't end up with a animate function that's hundreds of lines. 
*/
var animate = function () {
	requestAnimationFrame( animate );

	// cube.rotation.x += 0.1;
	// cube.rotation.y += 0.1;

	renderer.render(scene, camera);
};

animate();

// Rotating
// https://threejs.org/docs/#manual/introduction/Matrix-transformations
box.rotation.x += 0.4;
box.rotation.y += 0.4;

//Note that matrixAutoUpdate must be set to false in this case, and you should make sure not to call updateMatrix. Calling updateMatrix will clobber the manual changes made to the matrix, recalculating the matrix from position, scale, and so on. 
box.matrixAutoUpdate = false;
var quaternion = new THREE.Quaternion();
quaternion.setFromAxisAngle( new THREE.Vector3( 0, 1, 0 ), Math.PI / 2 );
box.matrix.setRotationFromQuaternion(quaternion);
// Rotate again
box.matrix.setPosition();