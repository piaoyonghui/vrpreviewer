/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var options = {
          x: 0,
          y: 0,
          width: window.screen.width,
          height: window.screen.height,
          camera: CameraPreview.CAMERA_DIRECTION.BACK,
          toBack: true,
          tapPhoto: true,
          tapFocus: false,
          previewDrag: false
        };

        CameraPreview.startCamera(options);

        // Initialize app
        var myApp = new Framework7();
         
        // If we need to use custom DOM library, let's save it to $$ variable:
        var $$ = Dom7;
         
        // Add view
        var mainView = myApp.addView('.view-main', {
          // Because we want to use dynamic navbar, we need to enable it for this view:
          dynamicNavbar: true
        });






        // //==============HammerJS=====================\\
        // var myElement = document.getElementById('myElement');

        // // create a simple instance
        // // by default, it only adds horizontal recognizers
        // var mc = new Hammer(myElement);

        // // listen to events...
        // mc.on("panleft panright tap press", function(ev) {
        //     myElement.textContent = ev.type +" gesture detected.";
        // });


        // <img id="myimage" src="http://placecage/1280/720">
        var image = document.getElementById('myimage');

        var mc = new Hammer.Manager(image);

        var pinch = new Hammer.Pinch();
        var pan = new Hammer.Pan();

        pinch.recognizeWith(pan);

        mc.add([pinch, pan]);

        var adjustScale = 1;
        var adjustDeltaX = 0;
        var adjustDeltaY = 0;

        var currentScale = null;
        var currentDeltaX = null;
        var currentDeltaY = null;

        var myElement = document.getElementById('myElement');
        // Prevent long press saving on mobiles.
        myElement.addEventListener('touchstart', function (e) {
            e.preventDefault()
        });

        // Handles pinch and pan events/transforming at the same time;
        mc.on("pinch pan", function (ev) {

            var transforms = [];

            // Adjusting the current pinch/pan event properties using the previous ones set when they finished touching
            currentScale = adjustScale * ev.scale;
            currentDeltaX = adjustDeltaX + (ev.deltaX / currentScale);
            currentDeltaY = adjustDeltaY + (ev.deltaY / currentScale);

            // Concatinating and applying parameters.
            transforms.push('scale(' + currentScale + ')');
            transforms.push('translate(' + currentDeltaX + 'px,' + currentDeltaY + 'px)');
            myElement.style.webkitTransform = transforms.join(' ');

        });


        mc.on("panend pinchend", function (ev) {

            // Saving the final transforms for adjustment next time the user interacts.
            adjustScale = currentScale;
            adjustDeltaX = currentDeltaX;
            adjustDeltaY = currentDeltaY;

        });

        //================================================================================
        $(".productImage").click(function () {
            $("#myimage").prop("src",$(this).prop("src"));
        });
        // $("#threeLink").click(function (){
        //     mainView.router.load({url:'three.html', ignoreCache: true, reload: true});
        //     // From https://threejs.org/docs/index.html#manual/introduction/Creating-a-scene
        //     var scene = new THREE.Scene();
        //     var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

        //     var renderer = new THREE.WebGLRenderer();
        //     renderer.setSize( window.innerWidth, window.innerHeight );
        //     document.body.appendChild( renderer.domElement );


        //     //==================================================================Insert images into faces ======================================================================================
        //     var textureLoader = new THREE.TextureLoader();

        //     var texture0 = textureLoader.load( 'img/paper-box-texture.jpg' );
        //     var texture1 = textureLoader.load( 'img/paper-box-texture.jpg' );
        //     var texture2 = textureLoader.load( 'img/paper-box-texture.jpg' );
        //     var texture3 = textureLoader.load( 'img/paper-box-texture.jpg' );
        //     var texture4 = textureLoader.load( 'img/paper-box-texture.jpg' );
        //     var texture5 = textureLoader.load( 'img/paper-box-texture.jpg' );

        //     var materials = [
        //         new THREE.MeshBasicMaterial( { map: texture0 } ),
        //         new THREE.MeshBasicMaterial( { map: texture1 } ),
        //         new THREE.MeshBasicMaterial( { map: texture2 } ),
        //         new THREE.MeshBasicMaterial( { map: texture3 } ),
        //         new THREE.MeshBasicMaterial( { map: texture4 } ),
        //         new THREE.MeshBasicMaterial( { map: texture5 } )
        //     ];
        //     var faceMaterial = new THREE.MeshFaceMaterial( materials );

        //     var geometry = new THREE.BoxGeometry( 1, 1, 1 );
        //     var box = new THREE.Mesh( geometry, faceMaterial );
        //     scene.add(box);

        //     camera.position.z = 5;

        //     var animate = function () {
        //         requestAnimationFrame( animate );

        //         // cube.rotation.x += 0.1;
        //         // cube.rotation.y += 0.1;

        //         renderer.render(scene, camera);
        //     };

        //     animate();

        //     // Rotating
        //     // https://threejs.org/docs/#manual/introduction/Matrix-transformations
        //     box.rotation.x += 0.4;
        //     box.rotation.y += 0.4;

        //     //Note that matrixAutoUpdate must be set to false in this case, and you should make sure not to call updateMatrix. Calling updateMatrix will clobber the manual changes made to the matrix, recalculating the matrix from position, scale, and so on. 
        //     box.matrixAutoUpdate = false;
        //     var quaternion = new THREE.Quaternion();
        //     quaternion.setFromAxisAngle( new THREE.Vector3( 0, 1, 0 ), Math.PI / 2 );
        //     box.matrix.setRotationFromQuaternion(quaternion);
        //     // Rotate again
        //     box.matrix.setPosition();


        // });
        

    }


};

app.initialize();